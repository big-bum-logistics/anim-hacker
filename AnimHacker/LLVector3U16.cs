﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimHacker
{
    public struct LLVector3U16
    {
        public UInt16 x;
        public UInt16 y;
        public UInt16 z;

        // Values From LSL Wiki
        private static float RAD_TO_DEG = 57.295779513082320876798154814105f;
        private static float DEG_TO_RAD = 0.017453292519943295769236907684886f;

        public override string ToString()
        {
            return "<" + x + "," + y + "," + z + ">";
        }

        public string ToHumanString(VectorType type)
        {
            float y = 1.5f;
            
            switch(type)
            {
                case VectorType.Position:
                    {
                        Console.WriteLine("To Human x " + x + " y " + y + " z " + z);
                        ToHuman(type, out var hx, out var hy, out var hz);
                        Console.WriteLine("hx " + hx + " hy " + hy + " hz " + hz);
                        return "<"
                            + (hx.ToSignPaddedString("0.###") + "m").PadRight(8, ' ') 
                            + ", " + (hy.ToSignPaddedString("0.###") + "m").PadRight(8, ' ') 
                            + ", " + (hz.ToSignPaddedString("0.###") + "m").PadRight(8, ' ') 
                            + ">";
                    }
                default:
                    {
                        ToHuman(type, out var hx, out var hy, out var hz);
                        return "<"
                            + (hx.ToSignPaddedString("0.###") + "°").PadRight(8, ' ') 
                            + ", " + (hy.ToSignPaddedString("0.###") + "°").PadRight(8, ' ') 
                            + ", " + (hz.ToSignPaddedString("0.###") + "°").PadRight(8, ' ') 
                            + ">";
                        //return this.ToString(); // TODO implement rotation display in ui
                    }
            }
        }

        public static LLVector3U16 Parse(string str)
        {
            str = str.Trim();
            string[] strVector = str.Split(new char[]{'<',',','>'}, StringSplitOptions.RemoveEmptyEntries);
            LLVector3U16 vector;
            vector.x = UInt16.Parse(strVector[0].Trim());
            vector.y = UInt16.Parse(strVector[1].Trim());
            vector.z = UInt16.Parse(strVector[2].Trim());
            return vector;
        }

        public void ToHuman(VectorType type, out float hx, out float hy, out float hz)
        {
            switch(type)
            {
                case VectorType.Position:
                    hx = ToHumanMeters(x);
                    hy = ToHumanMeters(y);
                    hz = ToHumanMeters(z);
                    break;
                default:
                    hx = ToHumanRot(x);
                    hy = ToHumanRot(y);
                    hz = ToHumanRot(z);
                    break;
                    //throw new NotImplementedException();
                //    hx = ToHumanMeters(x);
                //    hy = ToHumanMeters(y);
                //    hz = ToHumanMeters(z);
                //    break;
                //default:
                //    throw new ArgumentException("type not valid");
            }
        }

        public void FromHuman(VectorType type, float hx, float hy, float hz)
        {
            switch(type)
            {
                case VectorType.Position:
                    x = FromHumanMeters(hx);
                    y = FromHumanMeters(hy);
                    z = FromHumanMeters(hz);
                    break;
                case VectorType.Rotation:
                    x = FromHumanRot(hx);
                    y = FromHumanRot(hy);
                    z = FromHumanRot(hz);
                    break;
                default:
                    throw new ArgumentException("type not valid");
            }
        }

        private float ToHumanMeters(UInt16 val)
        {
            Console.WriteLine("To human meters " + val);
            float human = ((((float)val) - 32767) / 32767) * 5;
            Console.WriteLine("Human " + human);
            return human;
        }

        private UInt16 FromHumanMeters(float humanM)
        {
            float bla = humanM / 5;
            bla = bla * 32767;
            bla += 32767;
            return (UInt16)Math.Round(bla,0); // todo
        }

        private float ToHumanRot(UInt16 val)
        {
            /*
             * With special thanks to Lucia Nightfire
             * For +/- 180 degrees:
             * = DEGREES(ASIN((# - (# > 32767) - 32767) / 32767)) * 2
             * where # is the 16 bit integer. 
            */
            return (RAD_TO_DEG * ((float)Math.Asin((((float)val) - 32767) / 32767) * 2)) ;
            //return human * RAD_TO_DEG;
        }

        private UInt16 FromHumanRot(float humanR)
        {
            float   pootRot = humanR * DEG_TO_RAD; // Undo RAD_TO_DEG
                    pootRot = pootRot / 2; // Undo *2
                    pootRot = (float)Math.Sin(pootRot); // Undo Math.Asin?? idk i'm not a mathematician
                    pootRot = pootRot * 32767; // Undo /32767
                    pootRot = pootRot + 32767; // Undo val - 32767
            // Should now have val??? :))
            return (UInt16)pootRot;
        }

    }
}
