﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimHacker
{
    public struct LLVector3F32
    {
        public float x;
        public float y;
        public float z;

        public override string ToString()
        {
            return "<" + x + "," + y + "," + z + ">";
        }

        public static LLVector3F32 Parse(string str)
        {
            str = str.Trim();
            string[] strVector = str.Split(new char[] { '<', ',', '>' },StringSplitOptions.RemoveEmptyEntries);
            LLVector3F32 vector;
            vector.x = float.Parse(strVector[0].Trim());
            vector.y = float.Parse(strVector[1].Trim());
            vector.z = float.Parse(strVector[2].Trim());
            return vector;
        }

        public static void ToHuman(out float x, out float y, out float z)
        {
            // TODO
            throw new NotImplementedException();
        }

        public static void FromHuman(float x, float y, float z)
        {
            // TODO
            throw new NotImplementedException();
        }
    }
}
