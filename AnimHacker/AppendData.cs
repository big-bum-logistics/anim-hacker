﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class AppendData : Form
    {
        Animation appendAnim = null;
        Animation toAnim = null;
        string appendToJoint = "";

        public AppendData()
        {
            InitializeComponent();
        }

        public AppendData(Animation appendAnim, Animation toAnim, string title, string appendToJoint)
        {
            InitializeComponent();
            this.appendAnim = appendAnim;
            this.toAnim = toAnim;
            this.Text = title;
            this.appendToJoint = appendToJoint;
        }

        public List<RotationKey> GetAppendedRotationData()
        {
            var timeOffset = float.Parse(textBox1.Text);
            var rawOffset = (ushort)toAnim.SecondsToRaw(timeOffset);
            List<RotationKey> keys = new List<RotationKey>();
            foreach(var key in ((JointMotion)listBox1.SelectedItem).rotationKeys)
            {
                ushort x = key.rot.x;
                ushort y = key.rot.y;
                ushort z = key.rot.z;
                if (checkBoxFlipX.Checked) x = flip(x);
                if (checkBoxFlipY.Checked) y = flip(y);
                if (checkBoxFlipZ.Checked) z = flip(z);
                var rot = new LLVector3U16
                {
                    x = x,
                    y = y,
                    z = z
                };
                var rotKey = new RotationKey
                {
                    rot = rot,
                    time = (ushort)(key.time + rawOffset)
                };
                keys.Add(rotKey);
            }
            return keys;
        }

        public List<PositionKey> GetAppendedPositionData()
        {
            var timeOffset = float.Parse(textBox1.Text);
            var rawOffset = (ushort)toAnim.SecondsToRaw(timeOffset);
            List<PositionKey> keys = new List<PositionKey>();
            foreach (var key in ((JointMotion)listBox1.SelectedItem).positionKeys)
            {
                ushort x = key.pos.x;
                ushort y = key.pos.y;
                ushort z = key.pos.z;
                if (checkBoxFlipX.Checked) x = flip(x);
                if (checkBoxFlipY.Checked) y = flip(y);
                if (checkBoxFlipZ.Checked) z = flip(z);
                var pos = new LLVector3U16
                {
                    x = x,
                    y = y,
                    z = z
                };
                var posKey = new PositionKey
                {
                    pos = pos,
                    time = (ushort)(key.time + rawOffset)
                };
                keys.Add(posKey);
            }
            return keys;
        }

        ushort flip(ushort val)
        {
            int x = val;
            x -= 32767;
            x *= -1;
            if (x <= 0) x = 0;
            if (x >= 65535) x = 65535;
            return (ushort)x;
        }

        private void AppendData_Load(object sender, EventArgs e)
        {
            buttonAppend.Enabled = false;
            foreach (var jointMotion in appendAnim.jointMotions)
            {
                listBox1.Items.Add(jointMotion);
                if (jointMotion.jointName == appendToJoint)
                {
                    listBox1.SelectedIndex = listBox1.Items.Count - 1;
                    buttonAppend.Enabled = true;
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                buttonAppend.Enabled = true;
            } else
            {
                buttonAppend.Enabled = false;
            }
        }
    }
}
