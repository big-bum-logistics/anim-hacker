﻿namespace AnimHacker
{
    partial class JointMotionEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPriority = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.listViewRotKeys = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelRotationKeys = new System.Windows.Forms.Label();
            this.listViewPositionKeys = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.buttonAddRotKey = new System.Windows.Forms.Button();
            this.buttonRemoveRotKey = new System.Windows.Forms.Button();
            this.buttonRemovePosKey = new System.Windows.Forms.Button();
            this.buttonAddPosKey = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonSortPosKeys = new System.Windows.Forms.Button();
            this.buttonSortRotKeys = new System.Windows.Forms.Button();
            this.buttonAppendPosKeysFromFile = new System.Windows.Forms.Button();
            this.buttonAppendRotKeysFromFile = new System.Windows.Forms.Button();
            this.buttonEditPosKey = new System.Windows.Forms.Button();
            this.buttonEditRotKey = new System.Windows.Forms.Button();
            this.listViewJointMotions = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonRemoveJoint = new System.Windows.Forms.Button();
            this.buttonAddJoint = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonRaw = new System.Windows.Forms.RadioButton();
            this.radioButtonDecoded = new System.Windows.Forms.RadioButton();
            this.button7 = new System.Windows.Forms.Button();
            this.buttonReplaceJointMotionsFromFile = new System.Windows.Forms.Button();
            this.buttonEditJointName = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelPriority
            // 
            this.labelPriority.AutoSize = true;
            this.labelPriority.Location = new System.Drawing.Point(6, 23);
            this.labelPriority.Name = "labelPriority";
            this.labelPriority.Size = new System.Drawing.Size(63, 13);
            this.labelPriority.TabIndex = 0;
            this.labelPriority.Text = "Joint Priority";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(168, 21);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(86, 20);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // listViewRotKeys
            // 
            this.listViewRotKeys.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewRotKeys.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewRotKeys.FullRowSelect = true;
            this.listViewRotKeys.HideSelection = false;
            this.listViewRotKeys.Location = new System.Drawing.Point(6, 82);
            this.listViewRotKeys.Name = "listViewRotKeys";
            this.listViewRotKeys.Size = new System.Drawing.Size(300, 335);
            this.listViewRotKeys.TabIndex = 3;
            this.listViewRotKeys.UseCompatibleStateImageBehavior = false;
            this.listViewRotKeys.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "time";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "rotation";
            this.columnHeader2.Width = 230;
            // 
            // labelRotationKeys
            // 
            this.labelRotationKeys.AutoSize = true;
            this.labelRotationKeys.Location = new System.Drawing.Point(6, 51);
            this.labelRotationKeys.Name = "labelRotationKeys";
            this.labelRotationKeys.Size = new System.Drawing.Size(73, 13);
            this.labelRotationKeys.TabIndex = 2;
            this.labelRotationKeys.Text = "Rotation Keys";
            // 
            // listViewPositionKeys
            // 
            this.listViewPositionKeys.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.listViewPositionKeys.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewPositionKeys.FullRowSelect = true;
            this.listViewPositionKeys.HideSelection = false;
            this.listViewPositionKeys.Location = new System.Drawing.Point(362, 82);
            this.listViewPositionKeys.Name = "listViewPositionKeys";
            this.listViewPositionKeys.Size = new System.Drawing.Size(300, 335);
            this.listViewPositionKeys.TabIndex = 8;
            this.listViewPositionKeys.UseCompatibleStateImageBehavior = false;
            this.listViewPositionKeys.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "time";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "position";
            this.columnHeader6.Width = 230;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(359, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Position Keys";
            // 
            // buttonAddRotKey
            // 
            this.buttonAddRotKey.Location = new System.Drawing.Point(312, 82);
            this.buttonAddRotKey.Name = "buttonAddRotKey";
            this.buttonAddRotKey.Size = new System.Drawing.Size(34, 23);
            this.buttonAddRotKey.TabIndex = 4;
            this.buttonAddRotKey.Text = "+";
            this.buttonAddRotKey.UseVisualStyleBackColor = true;
            this.buttonAddRotKey.Click += new System.EventHandler(this.buttonAddRotKey_Click);
            // 
            // buttonRemoveRotKey
            // 
            this.buttonRemoveRotKey.Location = new System.Drawing.Point(312, 111);
            this.buttonRemoveRotKey.Name = "buttonRemoveRotKey";
            this.buttonRemoveRotKey.Size = new System.Drawing.Size(34, 23);
            this.buttonRemoveRotKey.TabIndex = 5;
            this.buttonRemoveRotKey.Text = "-";
            this.buttonRemoveRotKey.UseVisualStyleBackColor = true;
            this.buttonRemoveRotKey.Click += new System.EventHandler(this.buttonRemoveRotKey_Click);
            // 
            // buttonRemovePosKey
            // 
            this.buttonRemovePosKey.Location = new System.Drawing.Point(668, 111);
            this.buttonRemovePosKey.Name = "buttonRemovePosKey";
            this.buttonRemovePosKey.Size = new System.Drawing.Size(34, 23);
            this.buttonRemovePosKey.TabIndex = 10;
            this.buttonRemovePosKey.Text = "-";
            this.buttonRemovePosKey.UseVisualStyleBackColor = true;
            this.buttonRemovePosKey.Click += new System.EventHandler(this.buttonRemovePosKey_Click);
            // 
            // buttonAddPosKey
            // 
            this.buttonAddPosKey.Location = new System.Drawing.Point(668, 82);
            this.buttonAddPosKey.Name = "buttonAddPosKey";
            this.buttonAddPosKey.Size = new System.Drawing.Size(34, 23);
            this.buttonAddPosKey.TabIndex = 9;
            this.buttonAddPosKey.Text = "+";
            this.buttonAddPosKey.UseVisualStyleBackColor = true;
            this.buttonAddPosKey.Click += new System.EventHandler(this.buttonAddPosKey_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonSortPosKeys);
            this.groupBox1.Controls.Add(this.buttonSortRotKeys);
            this.groupBox1.Controls.Add(this.buttonAppendPosKeysFromFile);
            this.groupBox1.Controls.Add(this.buttonAppendRotKeysFromFile);
            this.groupBox1.Controls.Add(this.buttonEditPosKey);
            this.groupBox1.Controls.Add(this.buttonEditRotKey);
            this.groupBox1.Controls.Add(this.labelPriority);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.buttonRemovePosKey);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.buttonAddPosKey);
            this.groupBox1.Controls.Add(this.listViewRotKeys);
            this.groupBox1.Controls.Add(this.buttonRemoveRotKey);
            this.groupBox1.Controls.Add(this.labelRotationKeys);
            this.groupBox1.Controls.Add(this.buttonAddRotKey);
            this.groupBox1.Controls.Add(this.listViewPositionKeys);
            this.groupBox1.Location = new System.Drawing.Point(207, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(708, 480);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Joint";
            // 
            // buttonSortPosKeys
            // 
            this.buttonSortPosKeys.Location = new System.Drawing.Point(597, 419);
            this.buttonSortPosKeys.Name = "buttonSortPosKeys";
            this.buttonSortPosKeys.Size = new System.Drawing.Size(65, 23);
            this.buttonSortPosKeys.TabIndex = 14;
            this.buttonSortPosKeys.Text = "Sort";
            this.buttonSortPosKeys.UseVisualStyleBackColor = true;
            this.buttonSortPosKeys.Click += new System.EventHandler(this.buttonSortPosKeys_Click);
            // 
            // buttonSortRotKeys
            // 
            this.buttonSortRotKeys.Location = new System.Drawing.Point(241, 419);
            this.buttonSortRotKeys.Name = "buttonSortRotKeys";
            this.buttonSortRotKeys.Size = new System.Drawing.Size(65, 23);
            this.buttonSortRotKeys.TabIndex = 13;
            this.buttonSortRotKeys.Text = "Sort";
            this.buttonSortRotKeys.UseVisualStyleBackColor = true;
            this.buttonSortRotKeys.Click += new System.EventHandler(this.buttonSortRotKeys_click);
            // 
            // buttonAppendPosKeysFromFile
            // 
            this.buttonAppendPosKeysFromFile.Location = new System.Drawing.Point(362, 419);
            this.buttonAppendPosKeysFromFile.Name = "buttonAppendPosKeysFromFile";
            this.buttonAppendPosKeysFromFile.Size = new System.Drawing.Size(102, 23);
            this.buttonAppendPosKeysFromFile.TabIndex = 12;
            this.buttonAppendPosKeysFromFile.Text = "Append from file...";
            this.buttonAppendPosKeysFromFile.UseVisualStyleBackColor = true;
            this.buttonAppendPosKeysFromFile.Click += new System.EventHandler(this.buttonAppendPosKeysFromFile_Click);
            // 
            // buttonAppendRotKeysFromFile
            // 
            this.buttonAppendRotKeysFromFile.Location = new System.Drawing.Point(6, 419);
            this.buttonAppendRotKeysFromFile.Name = "buttonAppendRotKeysFromFile";
            this.buttonAppendRotKeysFromFile.Size = new System.Drawing.Size(102, 23);
            this.buttonAppendRotKeysFromFile.TabIndex = 2;
            this.buttonAppendRotKeysFromFile.Text = "Append from file...";
            this.buttonAppendRotKeysFromFile.UseVisualStyleBackColor = true;
            this.buttonAppendRotKeysFromFile.Click += new System.EventHandler(this.buttonAppendRotKeysFromFile_Click);
            // 
            // buttonEditPosKey
            // 
            this.buttonEditPosKey.Location = new System.Drawing.Point(668, 140);
            this.buttonEditPosKey.Name = "buttonEditPosKey";
            this.buttonEditPosKey.Size = new System.Drawing.Size(34, 23);
            this.buttonEditPosKey.TabIndex = 11;
            this.buttonEditPosKey.Text = "Edit";
            this.buttonEditPosKey.UseVisualStyleBackColor = true;
            this.buttonEditPosKey.Click += new System.EventHandler(this.buttonEditPosKey_Click);
            // 
            // buttonEditRotKey
            // 
            this.buttonEditRotKey.Location = new System.Drawing.Point(312, 140);
            this.buttonEditRotKey.Name = "buttonEditRotKey";
            this.buttonEditRotKey.Size = new System.Drawing.Size(34, 23);
            this.buttonEditRotKey.TabIndex = 6;
            this.buttonEditRotKey.Text = "Edit";
            this.buttonEditRotKey.UseVisualStyleBackColor = true;
            this.buttonEditRotKey.Click += new System.EventHandler(this.buttonEditRotKey_Click);
            // 
            // listViewJointMotions
            // 
            this.listViewJointMotions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.listViewJointMotions.FullRowSelect = true;
            this.listViewJointMotions.HideSelection = false;
            this.listViewJointMotions.Location = new System.Drawing.Point(12, 20);
            this.listViewJointMotions.Name = "listViewJointMotions";
            this.listViewJointMotions.Size = new System.Drawing.Size(151, 434);
            this.listViewJointMotions.TabIndex = 0;
            this.listViewJointMotions.UseCompatibleStateImageBehavior = false;
            this.listViewJointMotions.View = System.Windows.Forms.View.Details;
            this.listViewJointMotions.SelectedIndexChanged += new System.EventHandler(this.listViewJointMotions_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Joint";
            this.columnHeader3.Width = 85;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Priority";
            this.columnHeader4.Width = 50;
            // 
            // buttonRemoveJoint
            // 
            this.buttonRemoveJoint.Location = new System.Drawing.Point(167, 49);
            this.buttonRemoveJoint.Name = "buttonRemoveJoint";
            this.buttonRemoveJoint.Size = new System.Drawing.Size(34, 23);
            this.buttonRemoveJoint.TabIndex = 3;
            this.buttonRemoveJoint.Text = "-";
            this.buttonRemoveJoint.UseVisualStyleBackColor = true;
            this.buttonRemoveJoint.Click += new System.EventHandler(this.buttonRemoveJoint_Click);
            // 
            // buttonAddJoint
            // 
            this.buttonAddJoint.Location = new System.Drawing.Point(167, 20);
            this.buttonAddJoint.Name = "buttonAddJoint";
            this.buttonAddJoint.Size = new System.Drawing.Size(34, 23);
            this.buttonAddJoint.TabIndex = 2;
            this.buttonAddJoint.Text = "+";
            this.buttonAddJoint.UseVisualStyleBackColor = true;
            this.buttonAddJoint.Click += new System.EventHandler(this.buttonAddJoint_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 498);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 57);
            this.panel1.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonRaw);
            this.groupBox2.Controls.Add(this.radioButtonDecoded);
            this.groupBox2.Location = new System.Drawing.Point(12, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 42);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "View";
            // 
            // radioButtonRaw
            // 
            this.radioButtonRaw.AutoSize = true;
            this.radioButtonRaw.Location = new System.Drawing.Point(81, 19);
            this.radioButtonRaw.Name = "radioButtonRaw";
            this.radioButtonRaw.Size = new System.Drawing.Size(47, 17);
            this.radioButtonRaw.TabIndex = 13;
            this.radioButtonRaw.Text = "Raw";
            this.radioButtonRaw.UseVisualStyleBackColor = true;
            this.radioButtonRaw.CheckedChanged += new System.EventHandler(this.viewRadioChanged);
            // 
            // radioButtonDecoded
            // 
            this.radioButtonDecoded.AutoSize = true;
            this.radioButtonDecoded.Checked = true;
            this.radioButtonDecoded.Location = new System.Drawing.Point(6, 19);
            this.radioButtonDecoded.Name = "radioButtonDecoded";
            this.radioButtonDecoded.Size = new System.Drawing.Size(69, 17);
            this.radioButtonDecoded.TabIndex = 12;
            this.radioButtonDecoded.TabStop = true;
            this.radioButtonDecoded.Text = "Decoded";
            this.radioButtonDecoded.UseVisualStyleBackColor = true;
            this.radioButtonDecoded.CheckedChanged += new System.EventHandler(this.viewRadioChanged);
            // 
            // button7
            // 
            this.button7.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button7.Location = new System.Drawing.Point(790, 22);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "&Close";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // buttonReplaceJointMotionsFromFile
            // 
            this.buttonReplaceJointMotionsFromFile.Location = new System.Drawing.Point(12, 460);
            this.buttonReplaceJointMotionsFromFile.Name = "buttonReplaceJointMotionsFromFile";
            this.buttonReplaceJointMotionsFromFile.Size = new System.Drawing.Size(151, 32);
            this.buttonReplaceJointMotionsFromFile.TabIndex = 1;
            this.buttonReplaceJointMotionsFromFile.Text = "Replace from file...";
            this.buttonReplaceJointMotionsFromFile.UseVisualStyleBackColor = true;
            this.buttonReplaceJointMotionsFromFile.Click += new System.EventHandler(this.buttonReplaceJointMotionsFromFile_Click);
            // 
            // buttonEditJointName
            // 
            this.buttonEditJointName.Enabled = false;
            this.buttonEditJointName.Location = new System.Drawing.Point(167, 78);
            this.buttonEditJointName.Name = "buttonEditJointName";
            this.buttonEditJointName.Size = new System.Drawing.Size(34, 23);
            this.buttonEditJointName.TabIndex = 4;
            this.buttonEditJointName.Text = "Edit";
            this.buttonEditJointName.UseVisualStyleBackColor = true;
            this.buttonEditJointName.Click += new System.EventHandler(this.buttonEditJointName_Click);
            // 
            // JointMotionEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(927, 555);
            this.ControlBox = false;
            this.Controls.Add(this.buttonEditJointName);
            this.Controls.Add(this.buttonReplaceJointMotionsFromFile);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonRemoveJoint);
            this.Controls.Add(this.buttonAddJoint);
            this.Controls.Add(this.listViewJointMotions);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "JointMotionEditor";
            this.Text = "JointMotionEditor";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelPriority;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ListView listViewRotKeys;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label labelRotationKeys;
        private System.Windows.Forms.ListView listViewPositionKeys;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonAddRotKey;
        private System.Windows.Forms.Button buttonRemoveRotKey;
        private System.Windows.Forms.Button buttonRemovePosKey;
        private System.Windows.Forms.Button buttonAddPosKey;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listViewJointMotions;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button buttonRemoveJoint;
        private System.Windows.Forms.Button buttonAddJoint;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button buttonEditPosKey;
        private System.Windows.Forms.Button buttonEditRotKey;
        private System.Windows.Forms.Button buttonReplaceJointMotionsFromFile;
        private System.Windows.Forms.Button buttonEditJointName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonRaw;
        private System.Windows.Forms.RadioButton radioButtonDecoded;
        private System.Windows.Forms.Button buttonAppendPosKeysFromFile;
        private System.Windows.Forms.Button buttonAppendRotKeysFromFile;
        private System.Windows.Forms.Button buttonSortPosKeys;
        private System.Windows.Forms.Button buttonSortRotKeys;
    }
}