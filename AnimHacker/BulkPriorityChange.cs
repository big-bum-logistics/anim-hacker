﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class BulkPriorityChange : Form
    {

        

        public BulkPriorityChange()
        {
            InitializeComponent();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void listView1_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Link;
            else e.Effect = DragDropEffects.None;
        }

        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            //MessageBox.Show("files len" + files.Length);
            if (files != null & files.Any())
            {
                foreach(var file in files)
                {
                    string fileName = Path.GetFileName(file);
                    ListViewItem lvi = new ListViewItem(fileName);
                    lvi.Tag = file;
                    listView1.Items.Add(lvi);
                }
            }
            labelDrop.Hide();
            buttonChange.Enabled = true;
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            int action = -1;
            const int ACTION_INCREMENT = 1;
            const int ACTION_DECREMENT = 2;
            const int ACTION_EXACT = 3;
            int priority = (int)numericUpDownPriority.Value;

            if (radioButtonIncreaseBy.Checked) action = ACTION_INCREMENT;
            if (radioButtonDecreaseBy.Checked) action = ACTION_DECREMENT;
            if (radioButtonSetToExactly.Checked) action = ACTION_EXACT;

            foreach(var lvi in listView1.Items)
            {
                string file = (string)((ListViewItem)lvi).Tag;
                Animation anim = Animation.LoadAnimation(file);
                switch(action)
                {
                    case ACTION_INCREMENT:
                        anim.header.priority += priority;
                        break;
                    case ACTION_DECREMENT:
                        anim.header.priority -= priority;
                        break;
                    case ACTION_EXACT:
                        anim.header.priority = priority;
                        break;
                }
                foreach (var joint in anim.jointMotions)
                {
                    switch(action)
                    {
                        case ACTION_INCREMENT:
                            joint.jointPriority += priority;
                            break;
                        case ACTION_DECREMENT:
                            joint.jointPriority -= priority;
                            break;
                        case ACTION_EXACT:
                            joint.jointPriority = priority;
                            break;
                    }
                }
                anim.SaveAs(file);
            }
            MessageBox.Show("Done");
            Close();

            
        }
    }
}
