﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class ConstraintEditor : Form
    {

        int viewing;
        private List<Constraint> constraints;

        public ConstraintEditor()
        {
            InitializeComponent();
        }

        public ConstraintEditor(List<Constraint> constraints)
        {
            InitializeComponent();
            this.constraints = constraints;
            refreshListView2();
            groupBox1.Enabled = false;
            
        }

        private void refreshListView2()
        {
            groupBox1.Enabled = false;
            listView2.Items.Clear();
            for (int i = 0; i < constraints.Count; i++)
            {
                string src = constraints[i].sourceVolume;
                string tgt = constraints[i].targetVolume;
                ListViewItem lvi = new ListViewItem((i + 1) + "");
                lvi.SubItems.Add(src);
                lvi.SubItems.Add(tgt);
                listView2.Items.Add(lvi);
            }
        }

        private void ViewConstraint(int num)
        {
            if (num >= 0 && num < constraints.Count)
            {
                groupBox1.Enabled = true;
                var constraint = constraints[num];
                constraint.DisplayOnListView(listView1);
                viewing = num;
            } else
            {
                groupBox1.Enabled = false;
            }
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            ViewConstraint(viewing - 1);
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            ViewConstraint(viewing + 1);
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                listView1.GetKeyValue(listView1.SelectedIndices[0], out string key, out object value);
                if (ValueEditor.TryGetNewValue(key, value, out var newValue))
                {
                    switch(key)
                    {
                        case "chainLength":
                            constraints[viewing].chainLength = (byte)newValue;
                            break;
                        case "constraintType":
                            constraints[viewing].constraintType = (byte)newValue;
                            break;
                        case "sourceVolume":
                            constraints[viewing].sourceVolume = (string)newValue;
                            break;
                        case "sourceOffset":
                            constraints[viewing].sourceOffset = (LLVector3F32)newValue;
                            break;
                        case "targetVolume":
                            constraints[viewing].targetVolume = (string)newValue;
                            break;
                        case "targetOffset":
                            constraints[viewing].targetOffset = (LLVector3F32)newValue;
                            break;
                        case "targetDir":
                            constraints[viewing].targetDir = (LLVector3F32)newValue;
                            break;
                        case "easeInStart":
                            constraints[viewing].easeInStart = (float)newValue;
                            break;
                        case "easeInStop":
                            constraints[viewing].easeInStop = (float)newValue;
                            break;
                        case "easeOutStart":
                            constraints[viewing].easeOutStart = (float)newValue;
                            break;
                        case "easeOutStop":
                            constraints[viewing].easeOutStop = (float)newValue;
                            break;
                    }
                    constraints[viewing].DisplayOnListView(listView1);
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count > 0)
            {
                ViewConstraint(listView2.SelectedIndices[0]);
            }
        }

        private void buttonAddConstraint_Click(object sender, EventArgs e)
        {
            Constraint constraint = new Constraint
            {
                chainLength = 2,
                constraintType = 0,
                sourceVolume = "R_FOOT",
                sourceOffset = new LLVector3F32 { x = 0, y = 0, z = 1 },
                targetVolume = "HEAD",
                targetOffset = new LLVector3F32 { x = 0, y = 0, z = 1 },
                targetDir = new LLVector3F32 { x = 0, y = 0, z = 0 },
                easeInStart = -1,
                easeInStop = 0,
                easeOutStart = 10,
                easeOutStop = 10
            };
            constraints.Add(constraint);
            refreshListView2();
            listView2.Items[constraints.Count - 1].Selected = true;
        }

        private void buttonRemoveConstraint_Click(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count > 0)
            {
                // What a dumb way I've done this
                constraints.Remove(constraints[listView2.SelectedIndices[0]]);
                refreshListView2();
                if (listView2.Items.Count > 0)
                {
                    listView2.Items[0].Selected = true;
                } else
                {
                    groupBox1.Enabled = false;
                }
            }
        }

        private void buttonReplaceConstraintsFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Animations (.anim)|*.anim|All Files (*.*)|*.*";
            ofd.Title = "Replace constraints with constraints from file...";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                string file = ofd.FileName;
                Animation a = Animation.LoadAnimation(file);
                constraints.Clear();
                constraints.AddRange(a.constraints);

                refreshListView2();
            }
        }
    }
}
