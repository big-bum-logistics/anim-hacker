﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class JointMotionEditor : Form
    {
        Animation anim;
        List<JointMotion> jointMotions;


        public JointMotionEditor()
        {
            InitializeComponent();
        }

        public JointMotionEditor(List<JointMotion> jointMotions, Animation anim)
        {
            InitializeComponent();
            groupBox1.Enabled = false;
            this.jointMotions = jointMotions;
            this.anim = anim;
            refreshJointMotionsLV(true);
        }

        private void refreshJointMotionsLV(bool select_first)
        {
            groupBox1.Enabled = false;
            listViewJointMotions.Items.Clear();
            foreach (var jointMotion in jointMotions)
            {
                ListViewItem lvi = new ListViewItem(jointMotion.jointName);
                lvi.Tag = jointMotion;
                lvi.SubItems.Add(jointMotion.jointPriority + "");
                listViewJointMotions.Items.Add(lvi);
            }
            if (listViewJointMotions.Items.Count > 0)
            {
                if (select_first)
                {
                    listViewJointMotions.Items[0].Selected = true;
                } else
                {
                    listViewJointMotions.Items[listViewJointMotions.Items.Count - 1].Selected = true;
                }
            }
        }

        private void listViewJointMotions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count > 0)
            {
                buttonEditJointName.Enabled = true;
                int idx = listViewJointMotions.SelectedIndices[0];
                var jointMotion = jointMotions[idx];

                numericUpDown1.Value = jointMotion.jointPriority;
                groupBox1.Text = jointMotion.jointName;

                listViewRotKeys.Items.Clear();
                listViewPositionKeys.Items.Clear();
                foreach (var rotKey in jointMotion.rotationKeys)
                {
                    ListViewItem lvi;
                    if (radioButtonDecoded.Checked) {
                        var seconds = anim.RawToSeconds(rotKey.time);
                        lvi = new ListViewItem(seconds.ToString("0.###") + "s");
                    }
                    else lvi = new ListViewItem(rotKey.time.ToString());
                    if (radioButtonDecoded.Checked) lvi.SubItems.Add(rotKey.rot.ToHumanString(VectorType.Rotation));
                    else lvi.SubItems.Add(rotKey.rot.ToString());
                    listViewRotKeys.Items.Add(lvi);
                }

                

                foreach (var posKey in jointMotion.positionKeys)
                {
                    ListViewItem lvi;
                    if (radioButtonDecoded.Checked) {
                        var seconds = anim.RawToSeconds(posKey.time);
                        lvi = new ListViewItem(seconds.ToString("0.###") + "s");
                    } 
                    else lvi = new ListViewItem(posKey.time.ToString());
                    if (radioButtonDecoded.Checked) lvi.SubItems.Add(posKey.pos.ToHumanString(VectorType.Position));
                    else lvi.SubItems.Add(posKey.pos.ToString());
                    listViewPositionKeys.Items.Add(lvi);
                }
                groupBox1.Enabled = true;
            } else
            {
                groupBox1.Enabled = false;
                buttonEditJointName.Enabled = false;
            }
        }

        private void buttonAddJoint_Click(object sender, EventArgs e)
        {
            if (ValueEditor.TryGetNewValue("Enter new joint name", "eg. mPelvis", out var newJointName))
            {
                JointMotion newMotion = new JointMotion
                {
                    jointName = (string)newJointName,
                    jointPriority = 3,
                    rotationKeys = new List<RotationKey>(),
                    positionKeys = new List<PositionKey>()
                };
                jointMotions.Add(newMotion);
                refreshJointMotionsLV(false);
            }
        }

        private void buttonRemoveJoint_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count > 0)
            {
                var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
                listViewJointMotions.SelectedItems[0].Tag = null;
                jointMotions.Remove(motion);
                refreshJointMotionsLV(true);
            }
        }

        private void buttonAddRotKey_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            object inTime;
            object inRot;
            if (radioButtonDecoded.Checked)
            {
                inTime = 0.0f;
                inRot   = new LLVector3U16 { x = 32767, y = 32767, z = 32767 };
            } else
            {
                inTime = "0";
                inRot  = "<32767,32767,32767>";
            }
            if (ValueEditor.TryGetNewValue("(1/2) Time", inTime, out object time)
                && ValueEditor.TryGetNewValue("(2/2) Rot", inRot, out object rot))
            {
                RotationKey rotKey;
                if (rot is string)
                {
                    rotKey = new RotationKey
                    {
                        rot = LLVector3U16.Parse((string)rot),
                        time = ushort.Parse((string)time)
                    };
                } else
                {
                    rotKey = new RotationKey
                    {
                        rot = (LLVector3U16)rot,
                        time = (ushort)anim.SecondsToRaw((float)time)
                    };
                }
                var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
                motion.rotationKeys.Add(rotKey);
                listViewJointMotions_SelectedIndexChanged(null, null);
            }
        }

        private void buttonAddPosKey_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            object inTime;
            object inPos;
            if (radioButtonDecoded.Checked)
            {
                inTime = 0.0f;
                inPos = new LLVector3U16 { x = 32767, y = 32767, z = 32767 };
            }
            else
            {
                inTime = "0";
                inPos = "<32767,32767,32767>";
            }
            if (ValueEditor.TryGetNewValue("(1/2) Time",inTime, out object time)
                && ValueEditor.TryGetNewValue("(2/2) Pos", inPos, out object pos))
            {
                PositionKey posKey;
                if (pos is string)
                {
                    posKey = new PositionKey
                    {
                        pos = LLVector3U16.Parse((string)pos),
                        time = ushort.Parse((string)time)
                    };
                } else
                {
                    posKey = new PositionKey
                    {
                        pos  = (LLVector3U16)pos,
                        time = (ushort)anim.SecondsToRaw((float)time)
                    };
                }   
                var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
                motion.positionKeys.Add(posKey);
                listViewJointMotions_SelectedIndexChanged(null, null);
            }
        }

        private void buttonRemoveRotKey_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            if (listViewRotKeys.SelectedItems.Count == 0) return;
            int idx = listViewRotKeys.SelectedIndices[0];
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            motion.rotationKeys.RemoveAt(idx);
            listViewRotKeys.Items.RemoveAt(idx);
        }

        private void buttonRemovePosKey_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            if (listViewPositionKeys.SelectedItems.Count == 0) return;
            int idx = listViewPositionKeys.SelectedIndices[0];
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            motion.positionKeys.RemoveAt(idx);
            listViewPositionKeys.Items.RemoveAt(idx);
        }

        private void buttonEditRotKey_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            if (listViewRotKeys.SelectedItems.Count == 0) return;
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            var rotKey = motion.rotationKeys[listViewRotKeys.SelectedIndices[0]];
            string mode;
            var out_time = 0;
            if (radioButtonRaw.Checked)
            {
                mode = " [Raw]";
                if (ValueEditor.TryGetNewValue("(1/2) Edit time" + mode, rotKey.time, out object time))
                {
                    out_time = int.Parse(time.ToString());
                }
                else
                {
                    return;
                }
            } else
            {
                mode = " [Decoded]";
                float decoded_time_in = anim.RawToSeconds(rotKey.time);
                if (ValueEditor.TryGetNewValue("(1/2) Edit time" + mode, decoded_time_in, out object decoded_time_out))
                {
                    out_time = (int)anim.SecondsToRaw((float)decoded_time_out);
                }
                else
                {
                    return;
                }

            }

            if (ValueEditor.TryGetNewValue("(2/2) Edit rot" + mode, rotKey.rot, out object rot))
            {
                rotKey.rot = (LLVector3U16)rot;
                rotKey.time = (ushort)out_time;
                listViewJointMotions_SelectedIndexChanged(null, null);
                //var lvi = listViewRotKeys.SelectedItems[0];
                //lvi.Text = time.ToString();
                //lvi.SubItems[1].Text = ((LLVector3U16)rot).ToHumanString(VectorType.Position);
            }

        }

        private void buttonEditPosKey_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            if (listViewPositionKeys.SelectedItems.Count == 0) return;
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            var posKey = motion.positionKeys[listViewPositionKeys.SelectedIndices[0]];
            var out_time = 0;
            string mode;
            if (radioButtonRaw.Checked)
            {
                mode = " [Raw]";
                if (ValueEditor.TryGetNewValue("(1/2) Edit time" + mode, posKey.time, out object time))
                {
                    out_time = int.Parse(time.ToString());
                }
                else
                {
                    return;
                }
            }
            else
            {
                mode = " [Decoded]";
                float decoded_time_in = anim.RawToSeconds(posKey.time);
                if (ValueEditor.TryGetNewValue("(1/2) Edit time" + mode, decoded_time_in, out object decoded_time_out))
                {
                    out_time = (int)anim.SecondsToRaw((float)decoded_time_out);
                }
                else
                {
                    return;
                }

            }

            if (ValueEditor.TryGetNewValue("(2/2) Edit pos" + mode, posKey.pos, out object pos))
            {
                posKey.pos = (LLVector3U16)pos;
                posKey.time = (ushort)out_time;
                listViewJointMotions_SelectedIndexChanged(null, null);
            }

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            motion.jointPriority = Convert.ToInt32(numericUpDown1.Value);
            listViewJointMotions.SelectedItems[0].SubItems[1].Text = motion.jointPriority + "";
        }

        private void buttonReplaceJointMotionsFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Animations (.anim)|*.anim|All Files (*.*)|*.*";
            ofd.Title = "Replace joint motions with joint motions from file...";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                string file = ofd.FileName;
                Animation a = Animation.LoadAnimation(file);
                jointMotions.Clear();
                jointMotions.AddRange(a.jointMotions);
                refreshJointMotionsLV(true);
            }
        }

        private void buttonEditJointName_Click(object sender, EventArgs e)
        {
            if (listViewJointMotions.SelectedItems.Count == 0) return;
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            if (ValueEditor.TryGetNewValue("Edit joint name", motion.jointName, out object newName))
            {
                motion.jointName = (string)newName;
                refreshJointMotionsLV(false);
            }
        }

        private void viewRadioChanged(object sender, EventArgs e)
        {
            listViewJointMotions_SelectedIndexChanged(null, null);
        }

        private void buttonAppendRotKeysFromFile_Click(object sender, EventArgs e)
        {
            try
            {
                var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
                var motionJointName = motion.jointName;
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Animations (.anim)|*.anim;*.animatn|All Files (*.*)|*.*";
                ofd.Title = "Append rot keys to " + motionJointName;
                DialogResult dr = ofd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var AppendDataDialog = new AppendData(Animation.LoadAnimation(ofd.FileName),anim, "Append Rot Keys to " + motionJointName, motionJointName);
                    if (AppendDataDialog.ShowDialog() == DialogResult.OK)
                    {
                        var keys = AppendDataDialog.GetAppendedRotationData();
                        foreach (var key in keys)
                        {
                            motion.rotationKeys.Add(key);
                        }
                    }
                    listViewJointMotions_SelectedIndexChanged(null, null);
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonAppendPosKeysFromFile_Click(object sender, EventArgs e)
        {
            try
            {
                var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
                var motionJointName = motion.jointName;
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Animations (.anim)|*.anim;*.animatn|All Files (*.*)|*.*";
                ofd.Title = "Append pos keys to " + motionJointName;
                DialogResult dr = ofd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var AppendDataDialog = new AppendData(Animation.LoadAnimation(ofd.FileName), anim, "Append Pos Keys to " + motionJointName, motionJointName);
                    if (AppendDataDialog.ShowDialog() == DialogResult.OK)
                    {
                        var keys = AppendDataDialog.GetAppendedPositionData();
                        foreach (var key in keys)
                        {
                            motion.positionKeys.Add(key);
                        }
                    }
                    listViewJointMotions_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonSortRotKeys_click(object sender, EventArgs e)
        {
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            motion.rotationKeys.Sort(delegate(RotationKey x, RotationKey y)
            {
                return x.time.CompareTo(y.time);
            });
            listViewJointMotions_SelectedIndexChanged(null, null);
        }

        private void buttonSortPosKeys_Click(object sender, EventArgs e)
        {
            var motion = (JointMotion)listViewJointMotions.SelectedItems[0].Tag;
            motion.positionKeys.Sort(delegate (PositionKey x, PositionKey y)
            {
                return x.time.CompareTo(y.time);
            });
            listViewJointMotions_SelectedIndexChanged(null, null);
        }
    }
}
