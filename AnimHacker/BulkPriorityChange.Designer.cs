﻿namespace AnimHacker
{
    partial class BulkPriorityChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.radioButtonIncreaseBy = new System.Windows.Forms.RadioButton();
            this.radioButtonDecreaseBy = new System.Windows.Forms.RadioButton();
            this.radioButtonSetToExactly = new System.Windows.Forms.RadioButton();
            this.numericUpDownPriority = new System.Windows.Forms.NumericUpDown();
            this.buttonChange = new System.Windows.Forms.Button();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelDrop = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPriority)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.labelDrop);
            this.panel1.Controls.Add(this.numericUpDownPriority);
            this.panel1.Controls.Add(this.radioButtonSetToExactly);
            this.panel1.Controls.Add(this.radioButtonDecreaseBy);
            this.panel1.Controls.Add(this.radioButtonIncreaseBy);
            this.panel1.Controls.Add(this.listView1);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 457);
            this.panel1.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.AllowDrop = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(13, 13);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(315, 315);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listView1_DragDrop);
            this.listView1.DragOver += new System.Windows.Forms.DragEventHandler(this.listView1_DragOver);
            // 
            // radioButtonIncreaseBy
            // 
            this.radioButtonIncreaseBy.AutoSize = true;
            this.radioButtonIncreaseBy.Checked = true;
            this.radioButtonIncreaseBy.Location = new System.Drawing.Point(17, 346);
            this.radioButtonIncreaseBy.Name = "radioButtonIncreaseBy";
            this.radioButtonIncreaseBy.Size = new System.Drawing.Size(80, 17);
            this.radioButtonIncreaseBy.TabIndex = 2;
            this.radioButtonIncreaseBy.TabStop = true;
            this.radioButtonIncreaseBy.Text = "Increase by";
            this.radioButtonIncreaseBy.UseVisualStyleBackColor = true;
            this.radioButtonIncreaseBy.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButtonDecreaseBy
            // 
            this.radioButtonDecreaseBy.AutoSize = true;
            this.radioButtonDecreaseBy.Location = new System.Drawing.Point(17, 369);
            this.radioButtonDecreaseBy.Name = "radioButtonDecreaseBy";
            this.radioButtonDecreaseBy.Size = new System.Drawing.Size(85, 17);
            this.radioButtonDecreaseBy.TabIndex = 3;
            this.radioButtonDecreaseBy.TabStop = true;
            this.radioButtonDecreaseBy.Text = "Decrease by";
            this.radioButtonDecreaseBy.UseVisualStyleBackColor = true;
            this.radioButtonDecreaseBy.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButtonSetToExactly
            // 
            this.radioButtonSetToExactly.AutoSize = true;
            this.radioButtonSetToExactly.Location = new System.Drawing.Point(17, 392);
            this.radioButtonSetToExactly.Name = "radioButtonSetToExactly";
            this.radioButtonSetToExactly.Size = new System.Drawing.Size(90, 17);
            this.radioButtonSetToExactly.TabIndex = 4;
            this.radioButtonSetToExactly.TabStop = true;
            this.radioButtonSetToExactly.Text = "Set to Exactly";
            this.radioButtonSetToExactly.UseVisualStyleBackColor = true;
            this.radioButtonSetToExactly.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // numericUpDownPriority
            // 
            this.numericUpDownPriority.Location = new System.Drawing.Point(17, 415);
            this.numericUpDownPriority.Name = "numericUpDownPriority";
            this.numericUpDownPriority.Size = new System.Drawing.Size(85, 20);
            this.numericUpDownPriority.TabIndex = 5;
            // 
            // buttonChange
            // 
            this.buttonChange.Enabled = false;
            this.buttonChange.Location = new System.Drawing.Point(219, 462);
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.Size = new System.Drawing.Size(108, 22);
            this.buttonChange.TabIndex = 6;
            this.buttonChange.Text = "&Change Priority";
            this.buttonChange.UseVisualStyleBackColor = true;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "File";
            this.columnHeader1.Width = 307;
            // 
            // labelDrop
            // 
            this.labelDrop.AutoSize = true;
            this.labelDrop.ForeColor = System.Drawing.SystemColors.GrayText;
            this.labelDrop.Location = new System.Drawing.Point(117, 159);
            this.labelDrop.Name = "labelDrop";
            this.labelDrop.Size = new System.Drawing.Size(103, 13);
            this.labelDrop.TabIndex = 7;
            this.labelDrop.Text = "Drop .anim files here";
            this.labelDrop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BulkPriorityChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 496);
            this.Controls.Add(this.buttonChange);
            this.Controls.Add(this.panel1);
            this.Name = "BulkPriorityChange";
            this.Text = "Priority Changer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPriority)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonSetToExactly;
        private System.Windows.Forms.RadioButton radioButtonDecreaseBy;
        private System.Windows.Forms.RadioButton radioButtonIncreaseBy;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.NumericUpDown numericUpDownPriority;
        private System.Windows.Forms.Button buttonChange;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label labelDrop;
    }
}