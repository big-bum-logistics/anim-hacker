﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class Form1 : Form
    {

        Animation anim;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            
        }


        private static PropertyInfo[] GetProperties(object obj)
        {
            return obj.GetType().GetProperties();
        }

        void readHeader2(BinaryReader reader)
        {
            int version = reader.ReadUInt16();
            int subVersion = reader.ReadUInt16();
            int priority = reader.ReadInt32();

            float duration = reader.ReadSingle();
            string name = reader.ReadString();
            float loopInPoint = reader.ReadSingle();
            float loopOutPoint = reader.ReadSingle();
            int loop = reader.ReadInt32();
            float easeInDuration = reader.ReadSingle();
            float easeOutDuration = reader.ReadSingle();
            uint numHandPoses = reader.ReadUInt32();
            uint numJointMotions = reader.ReadUInt32();

            addListViewItem("version", version);
            addListViewItem("subVersion", subVersion);
            addListViewItem("priority", priority);
            addListViewItem("duration", duration);
            addListViewItem("emote_name", name);
            addListViewItem("loopInPoint", loopInPoint);
            addListViewItem("loopOutPoint", loopOutPoint);
            addListViewItem("loop", loop);
            addListViewItem("easeInDuration", easeInDuration);
            addListViewItem("easeOutDuration", easeOutDuration);
            addListViewItem("numHandPoses", numHandPoses);
            addListViewItem("numJointMotions", numJointMotions);

        }


        // Returns the total length of the header.
        int headerToListView(byte[] bytes)
        {
            #region 1. Get the values
            int version = BitConverter.ToUInt16(bytes, 0); // 2 bytes, 0,1
            int subVersion = BitConverter.ToUInt16(bytes, 2); // 2 bytes, 2,3
            int priority = BitConverter.ToInt32(bytes, 4);         // 4 bytes, 4,5,6,7
            MessageBox.Show("Version: " + version + " subversion " + subVersion + " priority " + priority);
            // Duration is a 32 bit float (8 bits is 1 byte.. so 4 byte float)
            float duration = BitConverter.ToSingle(bytes, 8); // 4 bytes, 8,9,10,11
            int strOffset = 0;
            int strLen = BitConverter.ToInt32(bytes, 12); // 4 bytes, 12,13,14,15
            strOffset += strLen;
            string emoteName = "NOT NAMED - 0 BYTES";
            if (strLen > 0)
            {
                emoteName = BitConverter.ToString(bytes, strLen);
            }
            float loopInPoint = BitConverter.ToSingle(bytes, strOffset + 16); // 4 bytes, 16,17,18,19 + strOffset
            float loopOutPoint = BitConverter.ToSingle(bytes, strOffset + 20); // 4 bytes, 20,21,22,23 + strOffset
            int loops = BitConverter.ToInt32(bytes, strOffset + 24); // 4 bytes, 24,25,26,27
            float easeInDuration = BitConverter.ToSingle(bytes, strOffset + 28); // 4 bytes, 28,29,30,31 + strOffset
            float easeOutDuration = BitConverter.ToSingle(bytes, strOffset + 32); // 4 bytes, 32,33,34,35 + strOffset
            uint numHandPoses = BitConverter.ToUInt32(bytes, strOffset + 36); // 4 bytes, 36,37,38,39 + strOffset
            uint numJointMotions = BitConverter.ToUInt32(bytes, strOffset + 40); // 4 bytes, 40,41,42,43
            #endregion
            #region 2. Populate the listView
            addListViewItem("version", version.ToString());
            addListViewItem("subVersion", subVersion.ToString());
            addListViewItem("priority", priority.ToString());
            addListViewItem("duration", duration.ToString());
            addListViewItem("emoteName", emoteName);
            addListViewItem("loopInPoint", loopInPoint.ToString());
            addListViewItem("loopOutPoint", loopOutPoint.ToString());
            addListViewItem("loops", loops.ToString());
            addListViewItem("easeInDuration", easeInDuration.ToString());
            addListViewItem("easeOutDuration", easeOutDuration.ToString());
            addListViewItem("numHandPoses", numHandPoses.ToString());
            addListViewItem("numJointMotions", numJointMotions.ToString());
            #endregion
            #region 3. Return the length of the header
            int length = 43 + strOffset;
            return length;
            #endregion
        }

        void addListViewItem(string name, string value)
        {
            ListViewItem lvi = new ListViewItem(name);
            lvi.SubItems.Add(value);
            listView1.Items.Add(lvi);
        }

        void addListViewItem(string name, object value)
        {
            ListViewItem lvi = new ListViewItem(name);
            lvi.SubItems.Add(value.ToString());
            listView1.Items.Add(lvi);
        }

        private void buttonConstraints_Click(object sender, EventArgs e)
        {
            var constraintEditor = new ConstraintEditor(anim.constraints);
            constraintEditor.ShowDialog();
            buttonConstraints.Text = "View Constraints (" + anim.constraints.Count + ")";
        }

        private void buttonJoints_Click(object sender, EventArgs e)
        {
            var jointMotionEditor = new JointMotionEditor(anim.jointMotions,anim);
            jointMotionEditor.ShowDialog();
            buttonJoints.Text = "View Joint Motions (" + anim.jointMotions.Count + ")";
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Animations (.anim)|*.anim;*.animatn|All Files (*.*)|*.*";
                DialogResult dr = ofd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    buttonSave.Enabled  = false;
                    groupBox1.Enabled   = false;
                    string file = ofd.FileName;
                    anim = Animation.LoadAnimation(file);
                    Header header = anim.header;
                    header.DisplayOnListView(listView1);

                    buttonJoints.Text = "View Joint Motions (" + anim.jointMotions.Count + ")";
                    buttonConstraints.Text = "View Constraints (" + anim.constraints.Count + ")";
                    buttonSave.Enabled = true;
                    groupBox1.Enabled = true;
                    labelFile.Text = file;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal Error:");
                Console.WriteLine(ex.ToString());
                MessageBox.Show(ex.Message, "Fatal Error");
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (anim != null)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Animation (*.anim)|*.anim";
                sfd.Title = "Save Animation";
                var result = sfd.ShowDialog();
                if (result == DialogResult.OK)
                {
                    try
                    {
                        string filename = sfd.FileName;
                        anim.SaveAs(filename);
                    } catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "An error ocurred");
                    }
                }
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0) return;
            int selectedIndex = listView1.SelectedIndices[0];
            listView1.GetKeyValue(selectedIndex, out string key, out object value);
            if (ValueEditor.TryGetNewValue(key, value, out object newValue))
            {
                listView1.SelectedItems[0].Tag = newValue;
                listView1.SelectedItems[0].SubItems[1].Text = newValue.ToString();
                anim.header.UpdateFromListView(listView1);
            }
        }

        private void buttonReplaceHeaderFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Animations (.anim)|*.anim|All Files (*.*)|*.*";
            ofd.Title = "Replace header with header from file...";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                string file = ofd.FileName;
                Animation a = Animation.LoadAnimation(file);
                anim.header = a.header;
                anim.header.DisplayOnListView(listView1);
            }
        }

        private void buttonPriorityChanger_Click(object sender, EventArgs e)
        {
            var dlg = new BulkPriorityChange();
            dlg.ShowDialog();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://aiaicapta.in/anim-hacker/");
            Process.Start(sInfo);
        }
    }
}
