﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class ValueEditor : Form
    {


        public ValueEditor()
        {
            InitializeComponent();
        }

        public ValueEditor(string valueName, object value)
        {
            InitializeComponent();
            Text = valueName;
            textBox1.Text = value.ToString();
        }

        public static bool TryGetNewValue(string valueName, object value, out object newValue)
        {
            try
            {
                //MessageBox.Show("Value Name " + valueName);
                if (value is LLVector3U16 && !valueName.Contains("Raw"))
                {
                    var v = (LLVector3U16)value;
                    VectorType vt = VectorType.Position;
                    if (valueName.ToLower().Contains("rot")) vt = VectorType.Rotation;
                    var vectorEditor = new VectorEditor(valueName, value, vt);
                    var dr = vectorEditor.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        newValue = vectorEditor.GetValue();
                        return true;
                    } else
                    {
                        newValue = null;
                        return false;
                    }
                } else 
                {
                    var valueEditor = new ValueEditor(valueName, value);
                    var dr = valueEditor.ShowDialog();

                    if (dr == DialogResult.OK)
                    {
                        string strVal = valueEditor.GetValue();
                        switch (value)
                        {
                            case UInt16 ui16:
                                newValue = UInt16.Parse(strVal);
                                return true;
                            case uint ui32:
                                newValue = uint.Parse(strVal);
                                return true;
                            case int i:
                                newValue = int.Parse(strVal);
                                return true;
                            case byte b:
                                newValue = byte.Parse(strVal);
                                return true;
                            case string s:
                                newValue = strVal;
                                return true;
                            case float f:
                                newValue = float.Parse(strVal);
                                return true;
                            case LLVector3U16 vector:
                                newValue = LLVector3U16.Parse(strVal);
                                return true;
                            case LLVector3F32 vector:
                                newValue = LLVector3F32.Parse(strVal);
                                return true;
                            default:
                                throw new ArgumentException("Unrecognised type");
                        }
                    }
                    else
                    {
                        newValue = null;
                        return false;
                    }
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                newValue = null;
                return false;
            }
        }

        public string GetValue()
        {
            return textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var clippy = new Clippy();
            clippy.Show();
        }

        private void ValueEditor_Shown(object sender, EventArgs e)
        {
            textBox1.Focus();
            textBox1.SelectAll();
        }
    }
}
