﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public partial class VectorEditor : Form
    {
        VectorType type;
        object value;


        public VectorEditor()
        {
            InitializeComponent();
        }

        public VectorEditor(string valueName, object value, VectorType type)
        {
            InitializeComponent();
            this.Text = valueName;
            this.value = value;
            this.type = type;
            switch(value)
            {
                case LLVector3U16 vector:
                    vector.ToHuman(type, out float x, out float y, out float z);
                    textBoxX.Text = x.ToString();
                    textBoxY.Text = y.ToString();
                    textBoxZ.Text = z.ToString();
                    break;
                case LLVector3F32 vector:
                    throw new ArgumentException("Not supported yet");
            }
            updateUnitStr(type);
        }

        private void updateUnitStr(VectorType type)
        {
            string unit = "E";
            switch(type)
            {
                case VectorType.Position:
                    unit = "m";
                    break;
                case VectorType.Rotation:
                    unit = "°";
                    break;
            }
            labelUnit0.Text = unit;
            labelUnit1.Text = unit;
            labelUnit2.Text = unit;
        }

        public static bool TryGetNewVector(string valueName, object value, VectorType type, out object newValue)
        {
            try
            {
                var vectorEditor = new VectorEditor(valueName, value, type);
                var dr = vectorEditor.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    newValue = vectorEditor.GetValue();
                    return true;
                }
                newValue = null;
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                newValue = null;
                return false;
            }
        }


        public object GetValue()
        {
            switch (value)
            {
                case LLVector3U16 vector:
                    vector.FromHuman(type, float.Parse(textBoxX.Text), float.Parse(textBoxY.Text), float.Parse(textBoxZ.Text));
                    return vector;
                case LLVector3F32 vector:
                    throw new ArgumentException("Not supported yet");
            }
            return value;
        }
    }

    public enum VectorType
    {
        Position,
        Rotation
    }
}
