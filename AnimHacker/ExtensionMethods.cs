﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    static class ExtensionMethods
    {
        public static LLVector3U16 ReadLLVector3U16(this System.IO.BinaryReader reader)
        {
            LLVector3U16 vector;
            vector.x = reader.ReadUInt16();
            vector.y = reader.ReadUInt16();
            vector.z = reader.ReadUInt16();
            return vector;
        }

        public static string ToSignPaddedString(this float x, string format)
        {
            string str = x.ToString(format);
            if (str.StartsWith("-")) return str;
            return " " + str;
        }

        public static void WriteLLVector3U16(this System.IO.BinaryWriter writer, LLVector3U16 vector)
        {
            writer.Write(vector.x);
            writer.Write(vector.y);
            writer.Write(vector.z);
        }

        public static LLVector3F32 ReadLLVector3F32(this System.IO.BinaryReader reader)
        {
            LLVector3F32 vector;
            vector.x = reader.ReadSingle();
            vector.y = reader.ReadSingle();
            vector.z = reader.ReadSingle();
            return vector;
        }

        public static void WriteLLVector3F32(this System.IO.BinaryWriter writer, LLVector3F32 vector)
        {
            writer.Write(vector.x);
            writer.Write(vector.y);
            writer.Write(vector.z);
        }

        public static string ReadNullTerminatedString(this System.IO.BinaryReader reader)
        {
            string str = "";
            char ch;
            while ((int)(ch = reader.ReadChar()) != 0)
                str = str + ch;
            return str;
        }

        public static void WriteNullTerminatedString(this System.IO.BinaryWriter writer, string s)
        {
            Encoding encoding = Encoding.ASCII;
            byte[] buffer = encoding.GetBytes(s);
            writer.Write(buffer);
            writer.Write((byte)0);
        }

        public static string ReadNullTerminatedString(this System.IO.BinaryReader reader, int fixedLength)
        {
            byte[] byteArray = reader.ReadBytes(fixedLength);
            return Encoding.ASCII.GetString(byteArray).Split('\0').First();

        }

        public static void WriteNullTerminatedString(this System.IO.BinaryWriter writer, string s, int fixedLength)
        {
            Encoding encoding = Encoding.ASCII;
            int i = 0;
            byte[] strBytes = encoding.GetBytes(s);
            if (strBytes.Length <= fixedLength)
            {
                byte[] buffer = new byte[fixedLength];
                for (i = 0; i < fixedLength; i++)
                {
                    if (i < strBytes.Length)
                    {
                        buffer[i] = strBytes[i];
                    } else
                    {
                        buffer[i] = (byte)0;
                    }
                }
                writer.Write(buffer);
            } else
            {
                throw new ArgumentOutOfRangeException("String longer than fixedLength " + fixedLength);
            }
        }

        public static void AddKeyValue(this System.Windows.Forms.ListView lv, string key, object value)
        {
            if (value == null) MessageBox.Show("Added null value for key " + key);
            ListViewItem lvi = new ListViewItem(key);
            lvi.Tag = value;
            lvi.SubItems.Add(value.ToString());
            lv.Items.Add(lvi);
        }

        public static void GetKeyValue(this System.Windows.Forms.ListView lv, int index, out string key, out object value)
        {
            ListViewItem lvi = lv.Items[index];
            if (lvi == null) MessageBox.Show("List view null");

            value = lvi.Tag;
            if (value == null) MessageBox.Show("Value null");
            key = lvi.Text;
            if (key == null) MessageBox.Show("Key null");
        }

        public static object GetValue(this System.Windows.Forms.ListView lv, string key)
        {
            for (int i = 0; i < lv.Items.Count; i++)
            {
                if (lv.Items[i].Text == key) return lv.Items[i].Tag;
            }
            throw new KeyNotFoundException(key);
        }
    }
}
