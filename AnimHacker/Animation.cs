﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimHacker
{
    public class Animation
    {
        public Header header;
        public List<JointMotion> jointMotions;
        public List<Constraint> constraints;


        public static Animation LoadAnimation(string file)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(file, FileMode.Open)))
            {
                Header header = Header.ReadHeader(reader);
                uint numJointMotions = reader.ReadUInt32();
                JointMotion[] jointMotions = JointMotion.ReadJointMotions(reader, numJointMotions);
                int numConstraints = reader.ReadInt32();
                Constraint[] constraints = Constraint.ReadConstraints(reader, numConstraints);
                return new Animation
                {
                    header = header,
                    jointMotions = jointMotions.ToList(),
                    constraints = constraints.ToList()
                };
            }
        }

        public void SaveAs(string file)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Create(file)))
            {
                header.WriteHeader(writer);
                JointMotion.WriteJointMotions(writer, jointMotions.ToArray());
                Constraint.WriteConstraints(writer, constraints.ToArray());
                writer.Flush();
            }
        }

        /// <summary>
        /// Based on the animation duration set in Header, convert the given seconds to the raw int value
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public int SecondsToRaw(float seconds)
        {
            if (header.duration == 0) return 0;
            float multiplier = ((float)seconds) / ((float)header.duration);
            int raw = (int)Math.Round(multiplier * 65535);
            if (raw < 0) raw = 0;
            if (raw > 65535) raw = 65535;
            return raw;
        }

        public float RawToSeconds(int raw)
        {
            float as_pc = ((float)raw) / ((float)65535);
            return as_pc * header.duration;
        }
    }

    public class Header
    {
        public UInt16 version;
        public UInt16 subVersion;
        public int priority;
        public float duration;
        public string name;
        public float loopInPoint;
        public float loopOutPoint;
        public int loop;
        public float easeInDuration;
        public float easeOutDuration;
        public uint numHandPoses;
        

        public static Header ReadHeader(BinaryReader reader)
        {
            return new Header
            {
                version = reader.ReadUInt16(),
                subVersion = reader.ReadUInt16(),
                priority = reader.ReadInt32(),

                duration = reader.ReadSingle(),
                name = reader.ReadNullTerminatedString(),
                loopInPoint = reader.ReadSingle(),
                loopOutPoint = reader.ReadSingle(),
                loop = reader.ReadInt32(),
                easeInDuration = reader.ReadSingle(),
                easeOutDuration = reader.ReadSingle(),
                numHandPoses = reader.ReadUInt32()
            };
        }

        public void WriteHeader(BinaryWriter writer)
        {
            writer.Write(version);
            writer.Write(subVersion);
            writer.Write(priority);
            writer.Write(duration);
            writer.WriteNullTerminatedString(name);
            writer.Write(loopInPoint);
            writer.Write(loopOutPoint);
            writer.Write(loop);
            writer.Write(easeInDuration);
            writer.Write(easeOutDuration);
            writer.Write(numHandPoses);
        }

        public void DisplayOnListView(ListView lv)
        {
            lv.Items.Clear();
            lv.AddKeyValue("version", version);
            lv.AddKeyValue("subVersion", subVersion);
            lv.AddKeyValue("priority", priority);
            lv.AddKeyValue("duration", duration);
            lv.AddKeyValue("emoteName", name);
            lv.AddKeyValue("loopInPoint", loopInPoint);
            lv.AddKeyValue("loopOutPoint", loopOutPoint);
            lv.AddKeyValue("loop", loop);
            lv.AddKeyValue("easeInDuration", easeInDuration);
            lv.AddKeyValue("easeOutDuration", easeOutDuration);
            lv.AddKeyValue("handPose", numHandPoses);
        }

        public void UpdateFromListView(ListView lv)
        {
            version = (UInt16)lv.GetValue("version");
            subVersion = (UInt16) lv.GetValue("subVersion");
            priority = (int) lv.GetValue("priority");
            duration = (float) lv.GetValue("duration");
            name = (string) lv.GetValue("emoteName");
            loopInPoint = (float) lv.GetValue("loopInPoint");
            loopOutPoint = (float) lv.GetValue("loopOutPoint");
            loop = (int) lv.GetValue("loop");
            easeInDuration = (float) lv.GetValue("easeInDuration");
            easeOutDuration = (float) lv.GetValue("easeOutDuration");
            numHandPoses = (uint) lv.GetValue("handPose");
        }


    }

    public class JointMotion
    {
        public string jointName;
        public int jointPriority;
        public List<RotationKey> rotationKeys;
        public List<PositionKey> positionKeys;


        public static JointMotion[] ReadJointMotions(BinaryReader reader, uint numMotions)
        {
            JointMotion[] motions = new JointMotion[numMotions];
            for (int i =  0; i < numMotions; i++)
            {
                motions[i] = new JointMotion
                {
                    jointName = reader.ReadNullTerminatedString(),
                    jointPriority = reader.ReadInt32(),
                };
                int numRotKeys = reader.ReadInt32();
                motions[i].rotationKeys = RotationKey.ReadRotationKeys(reader, numRotKeys).ToList();
                int numPosKeys = reader.ReadInt32();
                motions[i].positionKeys = PositionKey.ReadPositionKeys(reader, numPosKeys).ToList();
                
            }
            return motions;
        }

        public static void WriteJointMotions(BinaryWriter writer, JointMotion[] jointMotions)
        {
            writer.Write((uint)jointMotions.Length);
            foreach (var jointMotion in jointMotions)
            {
                writer.WriteNullTerminatedString(jointMotion.jointName);
                writer.Write(jointMotion.jointPriority);
                RotationKey.WriteRotationKeys(writer, jointMotion.rotationKeys.ToArray());
                PositionKey.WritePositionKeys(writer, jointMotion.positionKeys.ToArray());
            }
            
        }

        public override string ToString()
        {
            return "[P" + jointPriority + "]  " + jointName;
        }
    }

    public class RotationKey
    {
        public UInt16 time; // TODO This is stored as bytes and not converted because no support for F16
        public LLVector3U16 rot;
        

        public static RotationKey[] ReadRotationKeys(BinaryReader reader, int numRotKeys)
        {
            //MessageBox.Show(numRotKeys + "");
            RotationKey[] rotationKeys = new RotationKey[numRotKeys];
            for (int i = 0; i < numRotKeys; i++)
            {
                rotationKeys[i] = new RotationKey
                {
                    time = reader.ReadUInt16(),
                    rot = reader.ReadLLVector3U16()
                };

            }
            return rotationKeys;
        }

        public static void WriteRotationKeys(BinaryWriter writer, RotationKey[] rotationKeys)
        {
            writer.Write(rotationKeys.Length);
            foreach(var rotationKey in rotationKeys)
            {
                writer.Write(rotationKey.time);
                writer.WriteLLVector3U16(rotationKey.rot);
            }
        }
    }

    public class PositionKey
    {
        public UInt16 time;
        public LLVector3U16 pos;

        public static PositionKey[] ReadPositionKeys(BinaryReader reader, int numPosKeys)
        {
            PositionKey[] positionKeys = new PositionKey[numPosKeys];
            for (int i = 0; i < numPosKeys; i++)
            {
                positionKeys[i] = new PositionKey
                {
                    time = reader.ReadUInt16(),
                    pos = reader.ReadLLVector3U16()
                };
            }
            return positionKeys;
        }

        public static void WritePositionKeys(BinaryWriter writer, PositionKey[] positionKeys)
        {
            writer.Write(positionKeys.Length);
            foreach(var positionKey in positionKeys)
            {
                writer.Write(positionKey.time);
                writer.WriteLLVector3U16(positionKey.pos);
            }
        }
    }

    public class Constraint
    {
        public byte    chainLength;
        public byte    constraintType;
        public string  sourceVolume;
        public LLVector3F32 sourceOffset;
        public string  targetVolume;
        public LLVector3F32 targetOffset;
        public LLVector3F32 targetDir;
        public float   easeInStart;
        public float   easeInStop;
        public float   easeOutStart;
        public float   easeOutStop;

        public static Constraint[] ReadConstraints(BinaryReader reader, int numConstraints)
        {
            Constraint[] constraints = new Constraint[numConstraints];
            for (int i = 0; i < numConstraints; i++)
            {
                constraints[i] = new Constraint
                {
                    chainLength = reader.ReadByte(),
                    constraintType = reader.ReadByte(),
                    sourceVolume = reader.ReadNullTerminatedString(16),
                    sourceOffset = reader.ReadLLVector3F32(),
                    targetVolume = reader.ReadNullTerminatedString(16),
                    targetOffset = reader.ReadLLVector3F32(),
                    targetDir = reader.ReadLLVector3F32(),
                    easeInStart = reader.ReadSingle(),
                    easeInStop = reader.ReadSingle(),
                    easeOutStart = reader.ReadSingle(),
                    easeOutStop = reader.ReadSingle()
                };
            }
            return constraints;
        }

        public static void WriteConstraints(BinaryWriter writer, Constraint[] constraints)
        {
            writer.Write(constraints.Length);
            foreach(var constraint in constraints)
            {
                writer.Write(constraint.chainLength);
                writer.Write(constraint.constraintType);
                writer.WriteNullTerminatedString(constraint.sourceVolume, 16);
                writer.WriteLLVector3F32(constraint.sourceOffset);
                writer.WriteNullTerminatedString(constraint.targetVolume, 16);
                writer.WriteLLVector3F32(constraint.targetOffset);
                writer.WriteLLVector3F32(constraint.targetDir);
                writer.Write(constraint.easeInStart);
                writer.Write(constraint.easeInStop);
                writer.Write(constraint.easeOutStart);
                writer.Write(constraint.easeOutStop);
            }
        }

        public void DisplayOnListView(ListView lv)
        {
            lv.Items.Clear();
            lv.AddKeyValue("chainLength", chainLength);
            lv.AddKeyValue("constraintType", constraintType);
            lv.AddKeyValue("sourceVolume", sourceVolume);
            lv.AddKeyValue("sourceOffset", sourceOffset);
            lv.AddKeyValue("targetVolume", targetVolume);

            lv.AddKeyValue("targetOffset", targetOffset);
            lv.AddKeyValue("targetDir", targetDir);
            lv.AddKeyValue("easeInStart", easeInStart);
            lv.AddKeyValue("easeInStop", easeInStop);
            lv.AddKeyValue("easeOutStart", easeOutStart);
            lv.AddKeyValue("easeOutStop", easeOutStop);
        }

    }
}
